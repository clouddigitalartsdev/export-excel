package excelGenerate;

import mz.co.financingmoz.beans.DynamicField;
import mz.co.financingmoz.beans.financing.Financing;
import mz.co.financingmoz.beans.institution.Institution;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;


/**
 * Created by Cloud on 5/29/2017.
 */
public class ExcelGenerate {


    private String FILE_NAME;


    public String getFILE_NAME() {
        return FILE_NAME;
    }

    public void setFILE_NAME(String FILE_NAME) {
        this.FILE_NAME = FILE_NAME;
    }



    XSSFWorkbook workbook = new XSSFWorkbook();
    public String generateInstitutionByFinincing(Collection<Institution> institutions){

        for (Institution institution : institutions){

            generateInstitution(institution);
        }

        return FILE_NAME;
    }
    public String generateInstitution(Institution institution){


        XSSFSheet sheet = workbook.createSheet(institution.getName());

        XSSFFont font1 = workbook.createFont();
        XSSFFont font2 = workbook.createFont();
        font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font1.setFontName(HSSFFont.FONT_ARIAL);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font1);

        XSSFCellStyle styleEmpty = workbook.createCellStyle();
        font2.setItalic(true);
        styleEmpty.setFont(font2);

        Row rowA0 = sheet.createRow(0);
        Row rowA1 = sheet.createRow(1);
        Row rowA2 = sheet.createRow(2);
        Row rowA3 = sheet.createRow(3);
        Row rowA4 = sheet.createRow(4);
        Row rowA5 = sheet.createRow(5);
        Row rowA6 = sheet.createRow(6);
        Row rowA7 = sheet.createRow(7);

        Row row3 = sheet.createRow(13);
        Cell cell01 = row3.createCell(0);
        Cell cell02 = row3.createCell(1);
        Cell cell03 = row3.createCell(2);
        Cell cell04 = row3.createCell(3);
        Cell cell05 = row3.createCell(4);
        Cell cell06 = row3.createCell(5);
        Cell cell07 = row3.createCell(6);
        Cell cell08 = row3.createCell(7);

        Cell cell1 = rowA0.createCell(0);
        Cell cell2 = rowA1.createCell(0);
        Cell cell3 = rowA2.createCell(0);
        Cell cell4 = rowA3.createCell(0);
        Cell cell5 = rowA4.createCell(0);
        Cell cell6 = rowA5.createCell(0);
        Cell cell7 = rowA6.createCell(0);
        Cell cell8 = rowA7.createCell(0);

        cell1.setCellValue("Nome");
        cell2.setCellValue("Email");
        cell3.setCellValue("Categoria");
        cell4.setCellValue("Sector");
        cell5.setCellValue("Contacto");
        cell6.setCellValue("Endereço");
        cell7.setCellValue("Website");
        cell8.setCellValue("Locais");

        cell1.setCellStyle(style);
        cell2.setCellStyle(style);
        cell3.setCellStyle(style);
        cell4.setCellStyle(style);
        cell5.setCellStyle(style);
        cell6.setCellStyle(style);
        cell7.setCellStyle(style);
        cell8.setCellStyle(style);

        Cell cell11 = rowA0.createCell(1);
        Cell cell12 = rowA1.createCell(1);
        Cell cell13 = rowA2.createCell(1);
        Cell cell14 = rowA3.createCell(1);
        Cell cell15 = rowA4.createCell(1);
        Cell cell16 = rowA5.createCell(1);
        Cell cell17 = rowA6.createCell(1);
        Cell cell18 = rowA7.createCell(1);

        cell1.setCellValue("Nome");
        cell2.setCellValue("Email");
        cell3.setCellValue("Categoria");
        cell4.setCellValue("Sector");
        cell5.setCellValue("Contacto");
        cell6.setCellValue("Endereço");
        cell7.setCellValue("Website");
        cell8.setCellValue("Locais");


        String locations= "";
        for (int k=0;k < institution.getLocations().size(); k++){

            locations += institution.getLocations().iterator().next().getName()+";";
        }

        cell11.setCellValue(institution.getName());
        cell12.setCellValue(institution.getEmail());
        cell13.setCellValue(institution.getInstitutionType().getName());
        cell14.setCellValue(institution.getSector().getName());
        cell15.setCellValue(institution.getContact());
        cell16.setCellValue(institution.getAddress());
        cell17.setCellValue(institution.getWebsite());
        cell18.setCellValue(locations);

        int rowFields = 8;
        for (DynamicField dynamicField : institution.getDynamicFields()){

            Row rowField = sheet.createRow(rowFields);
            Cell cellField0 = rowField.createCell(0);
            Cell cellField1 = rowField.createCell(1);

            cellField0.setCellStyle(style);

            cellField0.setCellValue(dynamicField.getField().getLabel());
            cellField1.setCellValue(dynamicField.getValue());

            rowFields++;
        }

        cell01.setCellStyle(style);
        cell02.setCellStyle(style);
        cell03.setCellStyle(style);
        cell04.setCellStyle(style);
        cell05.setCellStyle(style);
        cell06.setCellStyle(style);
        cell07.setCellStyle(style);
        cell08.setCellStyle(style);

        cell01.setCellValue("Nome");
        cell02.setCellValue("Classe");
        cell03.setCellValue("Valor Minimo");
        cell04.setCellValue("Valor Maximo");
        cell05.setCellValue("Moeda");
        cell06.setCellValue("Taxa de Juri");
        cell07.setCellValue("Observações");


        Row subTitle = sheet.createRow(11);


        Cell cellTitle = subTitle.createCell(4);
        cellTitle.setCellStyle(style);
        cellTitle.setCellValue("Financiamentos / Produtos");



        Object[][] datatypes = new Object[institution.getFinancings().size()][9];


        int i  = 0;

        for (Financing financing : institution.getFinancings()){

            datatypes[i][0] = financing.getName();
            datatypes[i][1] = financing.getFinancingClass().getName();
            datatypes[i][2] = financing.getMinimumValue();
            datatypes[i][3] = financing.getMaximumValue();
            datatypes[i][4] = financing.getCurrency().getName();
            datatypes[i][5] = financing.getInterestRate().getIndexante() + financing.getInterestRate().getTaxaPrime() + financing.getInterestRate().getSpread();
            datatypes[i][6] = financing.getObs();

            i++;
        }

        int rowNum = 14;

        for (Object[] datatype : datatypes) {

            Row row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : datatype) {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                }
                else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
                else if (field instanceof Long) {
                    cell.setCellValue((Long) field);
                }
                else if (field instanceof Double) {
                    cell.setCellValue((Double) field);
                }
            }
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
            workbook.write(outputStream);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  FILE_NAME;
    }

    public String generateAllInstitution(Collection<Institution> institutions){

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Instituições");

        XSSFFont font1 = workbook.createFont();
        font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font1.setFontName(HSSFFont.FONT_ARIAL);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font1);

        Row row1 = sheet.createRow(0);

        Cell cell1 = row1.createCell(0);
        Cell cell2 = row1.createCell(1);
        Cell cell3 = row1.createCell(2);
        Cell cell4 = row1.createCell(3);
        Cell cell5 = row1.createCell(4);
        Cell cell6 = row1.createCell(5);
        Cell cell7 = row1.createCell(6);
        Cell cell8 = row1.createCell(7);

        cell1.setCellValue("Email");
        cell2.setCellValue("Nome");
        cell3.setCellValue("Categoria");
        cell4.setCellValue("Sector");
        cell5.setCellValue("Contacto");
        cell6.setCellValue("Endereço");
        cell7.setCellValue("Website");
        cell8.setCellValue("Locais");

        cell1.setCellStyle(style);
        cell2.setCellStyle(style);
        cell3.setCellStyle(style);
        cell4.setCellStyle(style);
        cell5.setCellStyle(style);
        cell6.setCellStyle(style);
        cell7.setCellStyle(style);
        cell8.setCellStyle(style);

        Object[][] datatypes = new Object[institutions.size()][9];


        int i  = 0;
        for (Institution institutionInfo : institutions){

            String locations= "";
                for (int k=0;k < institutionInfo.getLocations().size(); k++){

                     locations += institutionInfo.getLocations().iterator().next().getName()+";";
                }


                datatypes[i][0] = institutionInfo.getEmail();
                datatypes[i][1] = institutionInfo.getName();
                datatypes[i][2] = institutionInfo.getInstitutionType().getName();
                datatypes[i][3] = institutionInfo.getSector().getName();
                datatypes[i][4] = institutionInfo.getContact();
                datatypes[i][5] = institutionInfo.getAddress();
                datatypes[i][6] = institutionInfo.getWebsite();
                datatypes[i][7] = locations;

            i++;
        }

        int rowNum = 1;

        for (Object[] datatype : datatypes) {

            Row row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : datatype) {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                }
                else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
                else if (field instanceof Long) {
                    cell.setCellValue((Long) field);
                }
            }
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
            workbook.write(outputStream);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  FILE_NAME;
    }


}
