package mz.co.financingmoz.daccess.impl.util;

import mz.co.talkcode.drawSQL.DrawSQL;
import mz.co.talkcode.drawSQL.DrawSQLException;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.Assert.fail;

/**
 * Created by Cloud on 8/5/2016.
 */
public class Connections {

    public void getConnection(String fileName){


        Connection connection =  null;
        try {

            connection = getJdbc();
            DrawSQL drawQL = new DrawSQL.Builder().fromSketch(new File("integration-tests/"+fileName)).build();
            drawQL.apply(connection);

        }catch (DrawSQLException | IOException | SQLException  ex){

            ex.printStackTrace();
            fail("Test failed before test logic : "+ex.getMessage());

        }
        finally {


            try {
                if (!connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public Connection getJdbc() throws SQLException {

        return DriverManager.getConnection("jdbc:h2:mem:test");

    }
}
