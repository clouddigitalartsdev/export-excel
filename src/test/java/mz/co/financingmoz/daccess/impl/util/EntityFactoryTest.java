package mz.co.financingmoz.daccess.impl.util;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityFactoryTest {

    public static EntityManagerFactory factory = null;

    public static EntityManager getEntityManager() {

        if (factory == null) {

            try {
                factory = Persistence.createEntityManagerFactory("financingTest");
                return factory.createEntityManager();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            return factory.createEntityManager();
        }
        return  null;

    }

    public static void close() {
        factory.close();
    }

}
