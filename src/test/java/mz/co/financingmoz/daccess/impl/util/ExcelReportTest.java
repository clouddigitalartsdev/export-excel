package mz.co.financingmoz.daccess.impl.util;

import excelGenerate.ExcelGenerate;
import mz.co.financingmoz.beans.institution.Institution;
import mz.co.financingmoz.dao.exception.DAOException;
import mz.co.financingmoz.dao.spec.InstitutionDAO;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Vector;

/**
 * Created by Cloud on 5/29/2017.
 */
@RunWith(CdiTestRunner.class)

public class ExcelReportTest extends BaseTestClass{

    @Inject
    private InstitutionDAO institutionDAO;

    Connections connections = new Connections();


    @Before
    public void prepareTest() {

        recreateDatabase();

    }


    @Test
    public void TestExcel(){
        connections.getConnection("institutions.txt");


        ExcelGenerate excelGenerate = new ExcelGenerate();

        Collection<Institution> institutions = new Vector<>();
        Institution institution = new Institution();
        try {
            institutions = institutionDAO.allActive();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        excelGenerate.generateInstitutionByFinincing(institutions);

    }
}
