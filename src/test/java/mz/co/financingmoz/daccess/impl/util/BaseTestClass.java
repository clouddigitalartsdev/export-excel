package mz.co.financingmoz.daccess.impl.util;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * Created by Cloud on 30/01/2017.
 */
public abstract class BaseTestClass {

    private static EntityFactoryTest contextProvider = null;

    private static String context = "test";

    protected String context() {

        return context;
    }


    @BeforeClass
    public static void init(){
        // this will give you a CdiContainer for Weld or OWB, depending on the jar you added

      try {

        EntityFactoryTest.getEntityManager();

      }catch (Exception e){

          e.printStackTrace();
      }
    }


    @AfterClass
    public static void afterTests(){

       // EntityFactoryTest.close();
    }


    public void recreateDatabase(){

        EntityFactoryTest.getEntityManager();

    }

//    public Connection getTestJDBCConnection() throws SQLException {
//
//        return DriverManager.getConnection("jdbc:h2:mem:test");
//
//    }

}
